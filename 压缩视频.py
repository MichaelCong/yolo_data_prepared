import os, cv2

def file_name(file_dir):

     cap = cv2.VideoCapture(file_dir)
     success, _ = cap.read()
     # 重新合成的视频在原文件夹，如果需要分开，可以修改file_n
     videowriter = cv2.VideoWriter(file_dir+".avi", cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 20, (1920,1080))
     while success:
            success, img1 = cap.read()
            try:
                img = cv2.resize(img1, (1920, 1080), interpolation=cv2.INTER_LINEAR)
                videowriter.write(img)
            except:
                 break

file_name("C:\\Users\\RENCONG\\Desktop\\1\\DJI_0318_ori.MP4")