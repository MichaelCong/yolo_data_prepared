import os
import xml.etree.ElementTree as ET

import yuhangcv

def convert(size, box):
    dw = 1./(size[0])
    dh = 1./(size[1])
    x = (box[0] + box[1])/2.0 - 1
    y = (box[2] + box[3])/2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def convert_annotation(in_file, save_file,classes):
    out_file = open(save_file, 'w')
    tree=ET.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
            continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))
        bb = convert((w,h), b)
        out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

def convert_voc2darknet(voc_xml_files,img_file2path,classes,save_path,img_list_file=None):
    ''' voc标签的xml信息转Darknet（YOLO）格式
    voc_xml_files(list): xml标签文件名称列表
    img_file2path(dict): 图片路径映射{filename:filepath,...}
    classes(list): 处理的类别
    save_path(str): 转换结果存储目录
    img_list_file(str): 图片列表文件路径
    '''
    yuhangcv.mkdir_or_exist(save_path)
    if not img_list_file:
        img_list_file = os.path.join(save_path,'image_list.txt')

    list_file = open(img_list_file, 'w')
    for in_file in voc_xml_files:
        filename = os.path.basename(in_file)[:-4]
        if filename not in img_file2path:
            print (in_file,'have not images')
            continue
        
        img_file = img_file2path[filename]
        list_file.write(img_file+'\n')
        save_file = os.path.join(save_path, filename + '.txt')
        convert_annotation(in_file,save_file,classes)
    list_file.close()
    
 voc_xml_files = 
 img_file2path = 
 classes = 
 save_path = 
 img_list_file = None